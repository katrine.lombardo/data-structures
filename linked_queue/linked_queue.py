class LinkedQueueNode:
    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link


class LinkedQueue:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail

    def enqueue(self, val):
        node = LinkedQueueNode(val)
        if self.head is None:
            self.head = node
            self.tail = node
        else:
            self.tail.link = node
            self.tail = node

    def dequeue(self):
        _value = self.head.value
        self.head = self.head.link
        if self.head is None:
            self.tail = None
        return _value
