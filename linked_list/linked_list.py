class LinkedListNode:
    def __init__(self, value=None, link=None):
        self.value = value
        self.link = link


class LinkedList:
    def __init__(self,
                 head=None,
                 tail=None):

        self.head = head
        self.tail = tail
        self._length = 0

    @property
    def length(self):
        return self._length

    def get(self, idx):
        if idx < 0:
            idx = self._length + idx
        _current_node = self.traverse(idx)
        return _current_node.value

    def traverse(self, idx):
        if idx >= self._length:
            raise IndexError("idx out of range")

        i = 0
        _current_node = self.head

        while i < idx:
            _current_node = _current_node.link
            i += 1
        return _current_node

    def insert(self, value, idx=None):
        _new_node = LinkedListNode(value, link=None)

        if idx is None:
            idx = self._length

        if (idx < 0):
            idx = self._length + idx

        if self.head is None:
            self.head = _new_node
            self.tail = _new_node
            self._length += 1
            return None

        elif (idx == self._length):
            self.tail.link = _new_node
            self.tail = _new_node
            self._length += 1
            return None

        elif idx == 0:
            _new_node.link = self.head
            self.head = _new_node
            self._length += 1
            return None

        else:
            this_idx_node = self.traverse(idx - 1)
            next_idx_node = this_idx_node.link

            _new_node.link = next_idx_node
            this_idx_node.link = _new_node

            self._length += 1
            return None

    def remove(self, idx=0):
        if idx == 0:
            _val = self.head.value
            self.head = self.head.link
            self._length -= 1

            return _val

        previous_idx_node = self.traverse(idx - 1)
        this_idx_node = previous_idx_node.link
        _val = this_idx_node.value
        next_idx_node = this_idx_node.link

        previous_idx_node.link = next_idx_node
        if next_idx_node is None:
            self.tail = previous_idx_node

        self._length -= 1
        return _val
